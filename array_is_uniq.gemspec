# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'array_is_uniq/version'

Gem::Specification.new do |spec|
  spec.name          = "array_is_uniq"
  spec.version       = ArrayIsUniq::VERSION
  spec.authors       = ["zedtux"]
  spec.email         = ["zedtux@zedroot.org"]
  spec.description   = %q{Implement the missing uniq? method on Array}
  spec.summary       = %q{Implement the missing uniq? method on Array}
  spec.homepage      = "https://github.com/zedtux/array_is_uniq"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "simplecov"
end
