require "array_is_uniq/version"

class Array
  def uniq?
    self.length == self.uniq.length
  end
end
