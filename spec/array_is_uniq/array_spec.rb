require 'spec_helper'

describe Array do

  describe "#uniq?" do
    context "when the Array is empy" do
      it "should return true" do
        [].uniq?.should be_true
      end
    end
    context "when the Array has only one item" do
      it "should return true" do
        [1].uniq?.should be_true
      end
    end
    context "when the Array has unique items" do
      it "should return true" do
        [1, 2, 3, 4].uniq?.should be_true
      end
    end
    context "when the Array has more than one the same item" do
      it "should return false" do
        [1, 2, 3, 1].uniq?.should be_false
      end
    end
  end
end
