# ArrayIsUniq

[![Build Status](https://travis-ci.org/zedtux/array_is_uniq.png?branch=master)](https://travis-ci.org/zedtux/array_is_uniq)

Implement the missing uniq? method on Ruby Array

## Installation

Add this line to your application's Gemfile:

    gem 'array_is_uniq', :require => 'array'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install array_is_uniq

## Usage

    [].uniq?      #=> true
    [1].uniq?     #=> true
    [1, 2].uniq?  #=> true
    [1, 1].uniq?  #=> false

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
